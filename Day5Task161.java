package com.bahasaJava.upcasting;
 
public abstract class Binatang {
 
 
 public void makan(){
  System.out.println("Makan");
 }
 
 public void bergerak(){
  System.out.println("Bergerak");
 }
 
 public void tidur(){
  System.out.println("Tidur");
 }
 
}