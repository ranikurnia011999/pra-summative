import java.util.HashMap;  
import java.util.Map;  

public class latihan_java {
    
    public static void main(String[] args){
        //Membuat Objek Dari HashMap
        HashMap<Integer,String> unp = new HashMap<>();
        
        //Menginputkan Data pada HashMap
        unp.put(10, "Rani");
        unp.put(20, "Budi");
        unp.put(30, "Andi");
        unp.put(40, "Ani");
        unp.put(50, "Joko");
        unp.put(60, "Siti");
        unp.put(70, "Putri");
        unp.put(80, "Putra");
        unp.put(90, "Hendra");
        unp.put(100, "Indra");
        

        for(Map.Entry map : unpi.entrySet()){
            System.out.println("Key: "+map.getKey()+ " Valuenya = "+map.getValue());
        }
    }
}
