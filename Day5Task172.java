public class Mahasiswa extends Entitas implements MyInterface, MyOtherInterface  {
 
    @Override
    
    public void berlari() {
       System.out.println("Mahasiswa berlari");
    }
 
    @Override
   
    public void makan() {
       System.out.println("Mahasiswa makan");
    }
    
    @Override
    
    public void tertawa() {
        System.out.println("Mahasiswa tertawa");
    }
    
    public static void main (String args []){
        Mahasiswa m = new Mahasiswa();
        m.berlari();
        m.makan();
        m.tertawa();
    }
 
    
}
