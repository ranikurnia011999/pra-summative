
class Komputer {
  String cekInfo() { 
    return "Ini berasal dari class Komputer"; 
  }
}

class Laptop extends Komputer {
  String cekInfo() { 
    return "Ini berasal dari class Laptop"; 
  }
}

class BelajarJava {
  public static void main(String args[]){
    
    Laptop laptopRani = new Laptop();
    System.out.println(laptopRani.cekInfo());
  
  }
}