package com.bahasaJava.upcasting;
 
public class Test {
 
 public static void main (String args []){
  
  Kambing kambing = new Kambing();
  Binatang binatang = (Binatang) kambing;
  binatang.makan();
  
 }
}