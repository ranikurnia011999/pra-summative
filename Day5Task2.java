public class TernaryOperator {
    public static void main(String[] args) {
        
        var nilai = 75;
        String ucapan = nilai >= 70 ? "Anda lulus" : "Anda tidak lulus"; 
 
        System.out.println(ucapan);
         
    }
}